# Automation priority: null
# Test case importance: Low
*** Settings ***
Resource	squash_resources.resource

*** Keywords ***
Test Setup
	${__TEST_SETUP}	Get Variable Value	${TEST SETUP}
	${__TEST_1_SETUP}	Get Variable Value	${TEST 1 SETUP}
	Run Keyword If	$__TEST_SETUP is not None	${__TEST_SETUP}
	Run Keyword If	$__TEST_1_SETUP is not None	${__TEST_1_SETUP}

Test Teardown
	${__TEST_TEARDOWN}	Get Variable Value	${TEST TEARDOWN}
	${__TEST_1_TEARDOWN}	Get Variable Value	${TEST 1 TEARDOWN}
	Run Keyword If	$__TEST_TEARDOWN is not None	${__TEST_TEARDOWN}
	Run Keyword If	$__TEST_1_TEARDOWN is not None	${__TEST_1_TEARDOWN}

*** Test Cases ***
Dummy
	[Setup]	Test Setup

	Given soma state
	When I do something
	Then I get an answer

	[Teardown]	Test Teardown